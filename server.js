var express=require('express');
var app=express();
var bodyParser=require('body-parser');
var jwt=require('jwt-simple');
var multer=require('multer');

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.json({
  type:'application/vnd.api+json'
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/authenticate', function(req, res) {
  getAuthenticate(req.body,function(recordset){
    if(recordset.length==0){
      res.send(false);
    }
    else{
      var token=jwt.encode(recordset,'1a2b3c4d5e');
      res.send(token);
    }

  })
})

app.post('/edit', function(req, res) {
  editUser(req.body,function(recordset){
    res.send(recordset);
  });
});

app.post('/register', function(req, res) {
  insertRegisterData(req.body,function(data){
    console.log(data);
    res.send(data);
  });
 })

 app.post('/resetPassword', function(req, res) {
   var email=sendEmail(req.body);
   res.send(email);
  })

  app.get('/getfault', function(req, res) {
    console.log(req.body);
    getFaultType(function(recordset){
      res.json(recordset);
    });
   })

   app.post('/newreport', function(req, res) {
    var faultid = req.body.selectedFaults.toString();
    createNewReport(req.body,faultid,function(recordset){
      console.log(recordset);
    });
    })

    app.post('/reportdetail', function(req, res) {
     console.log(req.body);
     getReportDetails(req.body,function(recordset){
       res.json(recordset);
     });
     })

     app.post('/faultdetail', function(req, res) {
      console.log(req.body);
      getFaultDetails(req.body,function(recordset){
        res.json(recordset);
      });
      })

      app.post('/savecomment', function(req, res) {
       console.log(req.body);
       enterComment(req.body,function(recordset){
         res.send(recordset);
       });
       })

       app.post('/loadcomment', function(req, res) {
        console.log(req.body);
        loadComment(req.body,function(recordset){
          res.json(recordset);
        });
        })

        app.post('/addvote', function(req, res) {
         console.log(req.body);
         addVote(req.body,function(recordset){
           res.json(recordset);
         });
         })

         app.post('/getvote', function(req, res) {
          console.log(req.body);
          getVoteCount(req.body,function(recordset){
            res.json(recordset);
          });
          })

          app.post('/report-datafor-map', function(req, res) {
           console.log(req.body);
           getMapData(function(recordset){
             res.json(recordset);
           });
           })


 var sql=require('mssql');
 var config = {
   user:'sa',
   password:'Myfootman2@',
   database:'angular',
   server:'localhost'
 };

function getAuthenticate(data,callback){
  console.log(data);
  var email=data.email;
  var password=data.password;

  var connection=new sql.Connection(config,function(err){
    if(err){
      console.log(err);
    }
    var request=new sql.Request(connection);
    request.query("select userGuid,phoneNumber,email,name,lastname,address from Users where email='"+email+"' and password='"+password+"'",function(err,recordset){
      if(err){
       console.log(err);
      }
      console.log(recordset);
      callback(recordset);
    });
  });

}

function insertRegisterData(data,callback){
  console.log(data);
  var connection=new sql.Connection(config,function(err){
    if(err){
      console.log(err);
    }

    var request=new sql.Request(connection);
    request.query("insert into Users (phoneNumber,email,password,token,name,lastname,address,created,lastLogin,updated) values ('"+data.phone+"','"+data.email+"','"+data.password+"','','"+data.fname+"','"+data.lname+"','"+data.address+"','"+data.created+"','"+data.lastLogin+"','"+data.updated+"')",function(err){
      if(err){
        console.log(err);
      }
      else{
      callback("Successfully saved data!!");
    }
    });

  });

}

function editUser(data,callback){
  console.log(data);
  var connection=new sql.Connection(config,function(err){
    if(err){
      console.log(err);
    }
    var request=new sql.Request(connection);
    request.query("update Users set phoneNumber='"+data.phoneNumber+"',email='"+data.email+"',name='"+data.fname+"',lastname='"+data.lname+"',address='"+data.address+"',updated='"+data.updated+"' where userGuid='"+data.userGuid+"' ",function(err){
      if(err){
        console.log(err);
      }
      else{
      callback("Successfully saved data!!");
    }
    });
  });

}

function sendEmail(data) {
  console.log(data);
  var email=require('./node_modules/emailjs/email');
  var server=email.server.connect({
    user:"giribsaal@gmail.com",
    password:'passwordstrength2',
    host:"smtp.gmail.com",
    ssl:true
  });

  server.send({
    text:"Hello Bishal",
    from:"Bishal",
    to:'John <sunamjohn@gmail.com>',
    cc:'',
    subject:'Greetings'
  },function(err,message){
     if(err){
       console.log(err);
       return "Error";
     }
     else{
       return "Email sent success !!!";
     }
  });

}

function getFaultType(callback){

  var connection=new sql.Connection(config,function(err){
    if(err){
      console.log(err);
    }
    var request=new sql.Request(connection);
    request.query("select * from fault",function(err,recordset){
      if(err){
       console.log(err);
      }
      console.log(recordset);
      callback(recordset);
    });
  });

}

function createNewReport(data,faultid,callback){
 console.log(data);
 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query("insert into Reports (description,latitude,longitude,visibility,userGuid,faultreference,created,statusid,reportTypeid) values ('"+data.details+"','"+data.geolat+"','"+data.geolong+"','"+data.radiovalue+"','"+data.userGuid+"','"+faultid+"','"+data.created+"',1,1)",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback("Successfully created report");
   });
 });
}

function getReportDetails(data,callback){
 console.log(data);

 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }

   new sql.Request(connection).query("select r.reportGuid,r.created,r.description,s.status,rp.reportType from Reports r,Status s,reportTypes rp where r.statusid=s.statusid and r.reportTypeid=rp.reportTypeid ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback(recordset);
   });

 });

}


function getFaultDetails(data,callback){
 console.log(data);

 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query(" select r.reportGuid,r.created,r.description,s.status,rp.reportType from Reports r,Status s,reportTypes rp where r.reportGuid='"+data.id+"' and r.statusid=s.statusid and r.reportTypeid=rp.reportTypeid ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback(recordset);
   });
 });

}

function enterComment(data,callback){
 console.log(data);
 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query("insert into Comments (comment,userGuid,captured,reportGuid) values ('"+data.comment+"','"+data.userGuid+"','"+data.captured+"','"+data.reportGuid+"')",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback("Successfully saved comment");
   });
 });
}

function loadComment(data,callback){
 console.log(data);

 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query(" select commentGuid,comment,captured from Comments where reportGuid='"+data.reportGuid+"' ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback(recordset);
   });
 });

}

function addVote(data,callback){
 console.log(data);

 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query(" insert into Votes (userGuid,reportGuid) values ('"+data.userGuid+"','"+data.reportGuid+"') ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback("Done");
   });
 });

}

function getVoteCount(data,callback){
 console.log(data);

 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query(" select count(voteGuid) as voteCount from Votes where reportGuid='"+data.reportGuid+"' ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback(recordset);
   });
 });

}

function getMapData(callback){
 
 var connection=new sql.Connection(config,function(err){
   if(err){
     console.log(err);
   }
   var request=new sql.Request(connection);
   request.query(" select reportGuid,description,latitude,longitude from Reports ",function(err,recordset){
     if(err){
      console.log(err);
     }
     callback(recordset);
   });
 });

}

app.listen(3000);
